//
//  WRLDSError.h
//  WRLDSSDK
//
//  Copyright © 2019 WRLDS. All rights reserved.
//

static NSString * const WRLDSErrorDomain = @"WRLDSErrorDomain";

typedef NS_ENUM(NSInteger, WRLDSError) {
    WRLDSErrorUnknown = -1,
    WRLDSErrorBluetoothPoweredOff = 100,
    WRLDSErrorNotConnectedToBall = 200,
    WRLDSErrorAlreadyConnectedToBall = 201,
    WRLDSErrorUnrecognizedDevice = 202,
    WRLDSErrorInvalidConnectionTimeout = 203,
    WRLDSErrorMissingDFULibrary = 300,
    WRLDSErrorDFUBusy = 301,
    WRLDSErrorDFUAborted = 302,
    WRLDSErrorDFUDownloadInvalid = 303,
    WRLDSErrorDFUBrokenDownload = 303,
};
