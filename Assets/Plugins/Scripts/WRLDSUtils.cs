﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;
using AOT;
using System.Collections.Generic;
using System.Linq;

public sealed class WRLDSUtils 
{

    public enum BluetoothState
    {
        LINK_LOSS,
        DISCONNECTED,
        CONNECTED,
        CONNECTING,
        DISCONNECTING
    }

    public enum ProductType
    {
        BALL,
        TRAMPOLINE,
        BOW_AND_ARROW,
        DRUMSTICK,
        BOXING_GLOVE,
        NONE
    }

    public enum Trick
    {
        INDETERMINATE,
        FRONTFLIP,
        BACKFLIP,
        RIGHTSPIN,
        LEFTSPIN,
        RIGHTFLIP,
        LEFTFLIP
    }

    public enum Axis
    {
        X,
        Y,
        Z,
        XYZ
    }

    public enum Direction
    {
        IDLE,
        FORWARD,
        BACKWARD,
        RIGHT,
        LEFT
    }

    public enum JumpHeightMode
    {
        ACCELERATION,
        TIME
    }

    public enum Intensity
    {
        SOFT, 
        MEDIUM, 
        HARD
    }

#if UNITY_ANDROID
    public class BluetoothDevice
    {
        string deviceName;
        string deviceAddress;

        public BluetoothDevice(string name, string adress)
        {
            deviceName = name;
            deviceAddress = adress;
        }

        public string getDeviceName()
        {
            return deviceName;
        }

        public string getDeviceAddress()
        {
            return deviceAddress;
        }
    }
#endif

    public class BLESettings
    {
        public bool AutoConnect = true;
        public int ConnectionInterval = 50;
        public bool BleConnectionInBackground = false;

        public override string ToString()
        {
            return "BLESettings: " + $"AutoConnect: {AutoConnect}, ConnectionInterval: {ConnectionInterval} ms, " +
                $"KeepBLEConnectionInBackground: {BleConnectionInBackground}";
        }

        public string[] ToStringArray()
        {
            return new string[] { AutoConnect.ToString(), ConnectionInterval.ToString(), BleConnectionInBackground.ToString() };
        }

        public BLESettings SetAutoConnect(bool isAutoConnected)
        {
            this.AutoConnect = isAutoConnected;
            return this;
        }
        public BLESettings SetConnectionInterval(int connectionInterval)
        {
            this.ConnectionInterval = connectionInterval;
            return this;
        }
        public BLESettings KeepBLEConnectionInBackground(bool isActive)
        {
            this.BleConnectionInBackground = isActive;
            return this;
        }
    }

#if UNITY_ANDROID
    public static float[] ConvertObjectToFloatArray(AndroidJavaObject rawObject)
    {
        AndroidJavaObject javaObject = rawObject.Get<AndroidJavaObject>("FloatData");
        float[] floatArray = AndroidJNIHelper.ConvertFromJNIArray<float[]>(javaObject.GetRawObject());

        rawObject.Dispose();
        javaObject.Dispose();
        return floatArray;
    }
#endif

#if UNITY_ANDROID
    public static BluetoothDevice GetJavaBluetoothClass(AndroidJavaObject bluetoothDeviceObject)
    {
        string deviceName = bluetoothDeviceObject.Get<string>("deviceName");
        string deviceAddress = bluetoothDeviceObject.Get<string>("deviceAddress");
        BluetoothDevice device = new BluetoothDevice(deviceName, deviceAddress);

        bluetoothDeviceObject.Dispose();
        return device;
    }
#endif

#if UNITY_ANDROID
    public static AndroidJavaObject ToJavaFloatArray(float[] values)
    {
        if (values == null)
            return null;

        AndroidJavaClass arrayClass = new AndroidJavaClass("java.lang.reflect.Array");
        AndroidJavaObject arrayObject = arrayClass.CallStatic<AndroidJavaObject>("newInstance", new AndroidJavaClass("java.lang.Float"), values.Count());

        for (int i = 0; i < values.Count(); ++i)
        {
            arrayClass.CallStatic("set", arrayObject, i, new AndroidJavaObject("java.lang.Float", values[i]));
        }

        arrayClass.Dispose();
        return arrayObject;
    }
#endif

#if UNITY_ANDROID
    public static float[] FromJavaFloatArray(AndroidJavaObject values)
    {
        IntPtr floatArray = values.GetRawObject();

        values.Dispose();
        return AndroidJNI.FromFloatArray(floatArray);
    }
#endif


#if UNITY_ANDROID
    //public static AndroidJavaObject ToJavaHashMap(Dictionary<string, string> parameters)
    //{
    //    using (AndroidJavaObject hashMap = new AndroidJavaObject("java.util.HashMap"))
    //    {
    //        // Call 'put' via the JNI instead of using helper classes to avoid:
    //        //  "JNI: Init'd AndroidJavaObject with null ptr!"
    //        IntPtr method_Put = AndroidJNIHelper.GetMethodID(hashMap.GetRawClass(), "put",
    //            "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");

    //        object[] args = new object[2];
    //        foreach (KeyValuePair<string, string> kvp in parameters)
    //        {
    //            using (AndroidJavaObject k = new AndroidJavaObject("java.lang.String", kvp.Key))
    //            {
    //                using (AndroidJavaObject v = new AndroidJavaObject("java.lang.String", kvp.Value))
    //                {
    //                    args[0] = k;
    //                    args[1] = v;
    //                    AndroidJNI.CallObjectMethod(hashMap.GetRawObject(), method_Put, AndroidJNIHelper.CreateJNIArgArray(args));
    //                }
    //            }
    //        }
    //        return hashMap;
    //    }
    //}

    public static AndroidJavaObject ConvertDictionaryToJavaMap<K,V>(Dictionary<K, V> dictionary)
    {
        AndroidJavaObject map = new AndroidJavaObject("java.util.HashMap");
        IntPtr putMethod = AndroidJNIHelper.GetMethodID(map.GetRawClass(), "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");
        foreach (var entry in dictionary)
        {
            AndroidJNI.CallObjectMethod(
                map.GetRawObject(),
                putMethod,
                AndroidJNIHelper.CreateJNIArgArray(new object[] { entry.Key, entry.Value })
            );
        }
        return map;
    }
#endif


#if UNITY_ANDROID
    public static List<string> FromJavaList(AndroidJavaObject javaList)
    {        
        if (javaList != null)
        {
            var list = new List<string>();

            var size = javaList.Call<int>("size");
            for (int i = 0; i < size; i++)
            {
                string item = javaList.Call<string>("get", i);
                list.Add(item);
            }

            javaList.Dispose();
            return list;
        }
        else
        {
            return new List<string>();
        }

    }
#endif

#if UNITY_ANDROID
    public static Dictionary<K,V> FromJavaHashMap<K,V>(AndroidJavaObject javaHashMap)
    {
        if (javaHashMap != null)
        {
            Dictionary<K, V> dictionary = new Dictionary<K, V>();

            AndroidJavaObject entrySet = javaHashMap.Call<AndroidJavaObject>("entrySet");
            int entrySetSize =  javaHashMap.Call<int>("size");

            for (int i = 0; i < entrySetSize; i++)
            {
                K key = entrySet.Call<K>("getKey");
                V value = entrySet.Call<V>("getValue");

                dictionary.Add(key, value);
            }

            javaHashMap.Dispose();
            entrySet.Dispose();
            return dictionary;
        }
        else
        {
            return new Dictionary<K, V>();
        }

    }
#endif

#if UNITY_ANDROID
    public static AndroidJavaObject ToJavaStringArray(string[] values)
    {
        if (values == null) return null;
        AndroidJavaClass arrayClass = new AndroidJavaClass("java.lang.reflect.Array");
        AndroidJavaObject arrayObject = arrayClass.CallStatic<AndroidJavaObject>("newInstance", new AndroidJavaClass("java.lang.String"), values.Count());
        for (int i = 0; i < values.Count(); ++i)
        {
            arrayClass.CallStatic("set", arrayObject, i, new AndroidJavaObject("java.lang.String", values[i]));
        }

        arrayClass.Dispose();
        return arrayObject;
    }
#endif


#if UNITY_ANDROID

    public static BounceLeaderboard GetLeaderBoard(string jsonResult)
    {
        BounceLeaderboard leaderboard = JsonUtility.FromJson<BounceLeaderboard>(jsonResult);
        return leaderboard;
    }

    //Generated through https://jsonutils.com/ 
    //! Getters and Setters have to be removed
    //[System.Serializable]
    [Serializable]
    public class BounceCountLeaderboard
    {
        public string nickName;
        public int bounceCount;
    }

    [Serializable]
    public class BounceForceLeaderboard
    {
        public string nickName;
        public string bounceForce;
    }

    [Serializable]
    public class BounceLeaderboard
    {
        public List<BounceCountLeaderboard> bounceCountLeaderboard;
        public List<BounceForceLeaderboard> bounceForceLeaderboard;
    }
#endif
}
